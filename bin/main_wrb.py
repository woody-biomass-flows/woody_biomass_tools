# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: May 2023
"""


"""
MAIN

Evaluate the WRB of the provided example
Austria 2015

"""

from std import Settings, Info, DataSet
import globset as g
from wrb import Wrb
import os



# Initialize overall settings
settings = Settings(
	# set the path + filename of the forestry data
    jfsq_file_pathname = os.path.join(".","forestry_data.xlsx"),
	# set the path + filename of the energy data
    ene_file_pathname = os.path.join(".","wood_energy_sources_uses.xlsx"),
	# set the path + filename of the input/output coefficients and SWE conversion factors
    coeff_file_pathname = os.path.join(".","input_output_coefficients.xlsx"),
	# set the path for the output file
    outfile_path = ".",
	# set the unit of the results
	# implemented only "thsm3SWE"
    unit = "thsm3SWE",
    )
	
# Initialize the information of the single result
info = Info.build(settings, g, "Austria", 2015)

# Read all the needed data in proper class instance
data = DataSet.get(settings, info, g)

# Evaluate the WRB
wrb = Wrb.calc(settings, info, g, data)

# Save the results
wrb.toxls(settings, info, g)

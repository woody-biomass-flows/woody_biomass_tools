# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

### Defines the class for the data of a Wood Resource Balance
### main table plus ancillary tables
### see: https://knowledge4policy.ec.europa.eu/publication/wood-resource-balances_en


import openpyxl as xl
from base import FloatUnit
import os


class Wrb(dict):
    """
    Class for the data of a WRB
    mutable
    """
    
    def __add__(self, other):
        """
        Cumulate WRBs item by item.

        Parameters
        ----------
        other : Wrb.

        Returns
        -------
        new : Wrb.

        """
        assert(type(self) == type(other)), "TypeError."
        new = Wrb()
        for k in other.keys():
            if k in self.keys():
                new[k] = self[k] + other[k]
            else:
                new[k] = other[k]
        return new
    
    @classmethod
    def calc(cls, settings, info, g, data):
        """
        Evaluate the WRB items.
        Ancillary values (not used in the Wrb tables) have keys starting with '_'.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.
        data : DataSet
            all input data.

        Returns
        -------
        s : Wrb
            all wood resource balance values for the tables
            plus ancillary values.
            All values are FloatUnit

        """
        # check if they have m3SWE unit (to implement)
       
        s = cls()
        # primary
        s['w_irwc_rem'] = data.material.data[g.irwc].production
        s['w_irwnc_rem'] = data.material.data[g.irwnc].production
        s['w_fwc_rem'] = data.material.data[g.fwc].production
        s['w_fwnc_rem'] = data.material.data[g.fwnc].production
        s['w_irwc_net'] = data.material.data[g.irwc].net_import
        s['w_irwnc_net'] = data.material.data[g.irwnc].net_import
        s['w_fw_net'] = data.material.data[g.fw].net_import
        bark_irwc_mat = data.material.data[g.irwc].bark(g) 
        bark_irwnc_mat = data.material.data[g.irwnc].bark(g) 
        bark_fwc_mat = data.material.data[g.fwc].bark(g) 
        bark_fwnc_mat = data.material.data[g.fwnc].bark(g) 
        bark_fw_mat = data.material.data[g.fw].bark(g) 
        s['w_bark'] = bark_irwc_mat.production +\
                         bark_irwnc_mat.production +\
                         bark_fwc_mat.production +\
                         bark_fwnc_mat.production +\
                         bark_irwc_mat.net_import +\
                         bark_irwnc_mat.net_import +\
                         bark_fw_mat.net_import
        
        # secondary
        s['w_sawres'] = data.material.data[g.sawc].io_qty(settings, info, g.out_sawbyp) +\
                            data.material.data[g.sawnc].io_qty(settings, info, g.out_sawbyp)
        s['w_othres'] = data.material.data[g.venshe].io_qty(settings, info, g.out_othres) +\
                            data.material.data[g.ply].io_qty(settings, info, g.out_othres) +\
                            data.material.data[g.partboa].io_qty(settings, info, g.out_othres) +\
                            data.material.data[g.osb].io_qty(settings, info, g.out_othres) +\
                            data.material.data[g.fibboa].io_qty(settings, info, g.out_othres)
        s['w_pel'] = data.material.data[g.pel].production +\
                        data.material.data[g.othagg].production
        s['w_black'] = data.material.data[g.mechpu].io_qty(settings, info, g.out_black) +\
                          data.material.data[g.chempu].io_qty(settings, info, g.out_black) +\
                          data.material.data[g.semichempu].io_qty(settings, info, g.out_black) +\
                          data.material.data[g.disspu].io_qty(settings, info, g.out_black)
        s['w_chip_net'] = data.material.data[g.chip].net_import
        s['w_othres_net'] = data.material.data[g.res].net_import
        s['w_pel_net'] = data.material.data[g.pel].net_import +\
                            data.material.data[g.othagg].net_import
        
        # pcw
        s['w_pcw'] = data.material.data[g.partboa].io_qty(settings, info, g.in_pcw) +\
                     data.material.data[g.osb].io_qty(settings, info, g.in_pcw) +\
                     data.energy.pcw
        
        # material
        s['w_sawc_ind'] = data.material.data[g.sawc].production
        s['w_sawnc_ind'] = data.material.data[g.sawnc].production
        s['w_venshe_ind'] = data.material.data[g.venshe].production
        s['w_ply_ind'] = data.material.data[g.ply].production
        s['w_partboa_ind'] = data.material.data[g.partboa].production +\
                                data.material.data[g.osb].production
        s['w_fibboa_ind'] = data.material.data[g.fibboa].production
        s['w_mechpu_ind'] = data.material.data[g.mechpu].production
        s['w_chempu_ind'] = data.material.data[g.chempu].production
        s['w_semichempu_ind'] = data.material.data[g.semichempu].production
        s['w_disspu_ind'] = data.material.data[g.disspu].production
        s['w_pel_ind'] = s['w_pel']
        
        # energy
        s['w_dirw'] = data.energy.direct
        s['w_indw'] = data.energy.indirect
        s['w_unkw'] = data.energy.unknown
        
        # footer
        s['w_tot_sou'] = s['w_irwc_rem'] +\
                           s['w_irwnc_rem'] +\
                    	   s['w_fwc_rem'] +\
                    	   s['w_fwnc_rem'] +\
                    	   s['w_irwc_net'] +\
                    	   s['w_irwnc_net'] +\
                    	   s['w_fw_net'] +\
                    	   s['w_bark'] +\
                    	   s['w_sawres'] +\
                    	   s['w_othres'] +\
                    	   s['w_pel'] +\
                    	   s['w_black'] +\
                    	   s['w_chip_net'] +\
                    	   s['w_othres_net'] +\
                    	   s['w_pel_net'] +\
                    	   s['w_pcw']
        s['w_tot_use'] = s['w_sawc_ind'] +\
                    	    s['w_sawnc_ind'] +\
                    	    s['w_venshe_ind'] +\
                    	    s['w_ply_ind'] +\
                    	    s['w_partboa_ind'] +\
                    	    s['w_fibboa_ind'] +\
                    	    s['w_mechpu_ind'] +\
                    	    s['w_chempu_ind'] +\
                    	    s['w_semichempu_ind'] +\
                    	    s['w_disspu_ind'] +\
                    	    s['w_pel_ind'] +\
                    	    s['w_dirw'] +\
                    	    s['w_indw'] +\
                    	    s['w_unkw']
        s['w_dif'] = s['w_tot_sou'] - s['w_tot_use']
        
        # summary tables
        tot_pri = s['w_irwc_rem'] +\
                    s['w_irwnc_rem'] +\
                    s['w_fwc_rem'] +\
                    s['w_fwnc_rem'] +\
                    s['w_irwc_net'] +\
                    s['w_irwnc_net'] +\
                    s['w_fw_net'] +\
                    s['w_bark']
        tot_sec = s['w_sawres'] +\
                   	s['w_othres'] +\
                   	s['w_pel'] +\
                   	s['w_black'] +\
                   	s['w_chip_net'] +\
                   	s['w_othres_net'] +\
               	    s['w_pel_net']
        s['s_pri'] = tot_pri
        s['s_sec'] = tot_sec
        s['s_pcw'] = s['w_pcw']
        tot_saw = s['w_sawc_ind'] +\
                    s['w_sawnc_ind']
        tot_pan = s['w_venshe_ind'] +\
                   	s['w_ply_ind'] +\
                   	s['w_partboa_ind'] +\
               	    s['w_fibboa_ind']
        tot_pu = s['w_mechpu_ind'] +\
                   s['w_chempu_ind'] +\
                   s['w_semichempu_ind'] +\
        	       s['w_disspu_ind']
        tot_ind = tot_saw + tot_pan + tot_pu
        s['s_saw'] = tot_saw
        s['s_pan'] = tot_pan
        s['s_pu'] = tot_pu
        
        tot_ene = s['w_dirw'] + s['w_indw'] + s['w_unkw']
        s['s_dirw'] = s['w_dirw']
        s['s_indw'] = s['w_indw']
        s['s_unkw'] = s['w_unkw']
        s['s_enetrans'] = data.energy.transformation
        s['s_eneind'] = data.energy.industrial
        s['s_eneres'] = data.energy.residential
        s['s_eneoth'] = data.energy.other

        s['_primary'] = tot_pri
        s['_secondary'] = tot_sec
        s['_material_wo_pell'] = tot_ind
        s['_material'] = tot_ind + s['w_pel_ind']
        s['_energy'] = tot_ene
        
        return s
            
    @property
    def share(self):
        """
        Shares for the summary tables in the WRB sheets.

        Returns
        -------
        d : dict
            of all the percentages to show in the WRB sheet.

        """
        d = dict()
        d['s_pri'] = self['s_pri'].share(self['w_tot_sou'])
        d['s_sec'] = self['s_sec'].share(self['w_tot_sou'])
        d['s_pcw'] = self['w_pcw'].share(self['w_tot_sou'])
        d['s_saw'] = self['s_saw'].share(self['_material_wo_pell'])
        d['s_pan'] = self['s_pan'].share(self['_material_wo_pell'])
        d['s_pu'] = self['s_pu'].share(self['_material_wo_pell'])
        d['s_dirw'] = self['w_dirw'].share(self['_energy'])
        d['s_indw'] = self['w_indw'].share(self['_energy'])
        d['s_unkw'] = self['w_unkw'].share(self['_energy'])
        d['s_enetrans'] = self['s_enetrans'].share(self['_energy'])
        d['s_eneind'] =  self['s_eneind'].share(self['_energy'])
        d['s_eneres'] =  self['s_eneres'].share(self['_energy'])
        d['s_eneoth'] =  self['s_eneoth'].share(self['_energy'])
        return d
    
    def nullify_jwee_uses(self):
        """
        Sets to None the JWEE Ux values in the WRB tables (not in data).
        To be used on "cumulative" WRB when the sum involves NREAP data too.

        Returns
        -------
        None.

        """
        self['s_enetrans'] = FloatUnit(None, None)
        self['s_eneind'] = FloatUnit(None, None)
        self['s_eneres'] = FloatUnit(None, None)
        self['s_eneoth'] = FloatUnit(None, None)
    


    def toxls(self, settings, info, g):
        """
        write a whole WRB to an xls file

        Parameters
        ----------
        info : TYPE
            DESCRIPTION.
        g : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        
        def writerow(ws, lis, irow, icol):
            """
            write a single row in the given excel file

            Parameters
            ----------
            ws : openpyxl worksheet
                where to write.
            lis : list
                element to insert into the cells.
            irow : integer
                row to write in.
            icol : integer
                column where to start from.

            Returns
            -------
            None.

            """
            for c,el in enumerate(lis):
                if type(el) == list:
                    val = el[0]
                    color = el[1]
                else:
                    val = el
                    color = None
                try:
                    valore = val.value if val.value is not None else 'NA'
                except:
                    valore = val
                d = ws.cell(row=irow, column=c+icol, value=valore)
                d.font = xl.styles.Font(name='Arial Narrow', size=10, color=color)
                try:
                    if abs(valore) < 5.01 and abs(valore) > 0.:
                        d.number_format = '0.0'
                except:
                    pass

        gray = '5A5656'
        filename = os.path.join(settings.outfile_path, 'WRB_' + str(info.year) + '.xlsx')
        try:
            wb = xl.load_workbook(filename)
        except:
            wb = xl.Workbook()
            
        try:
            ws = wb[info.country]
        except:
            ws = wb.create_sheet(title=info.country)
        
        # write the titles
        ws.cell(row=1, column=1, value='Wood Resource Balance ' + str(info.year))
        ws.cell(row=1, column=9, value='Summary wood supply and use ' + str(info.year))
        c = 2
        r = 3
        row = [g.w_irwc_rem, round(self['w_irwc_rem'],1), None, 
               round(self['w_sawc_ind'],1), g.w_sawc_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_irwnc_rem, round(self['w_irwnc_rem'],1), None, 
               round(self['w_sawnc_ind'],1), g.w_sawnc_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_fwc_rem, round(self['w_fwc_rem'],1), None, 
               round(self['w_venshe_ind'],1), g.w_venshe_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_fwnc_rem, round(self['w_fwnc_rem'],1), None, 
               round(self['w_ply_ind'],1), g.w_ply_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_irwc_net, round(self['w_irwc_net'],1), None, 
               round(self['w_partboa_ind'],1), g.w_partboa_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_irwnc_net, round(self['w_irwnc_net'],1), None, 
               round(self['w_fibboa_ind'],1), g.w_fibboa_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_fw_net, round(self['w_fw_net'],1), None, 
               round(self['w_mechpu_ind'],1), g.w_mechpu_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_bark, round(self['w_bark'],1), None, 
               round(self['w_chempu_ind'],1), g.w_chempu_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_sawres, round(self['w_sawres'],1), None, 
               round(self['w_semichempu_ind'],1), g.w_semichempu_ind]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_othres, round(self['w_othres'],1), None, 
               round(self['w_disspu_ind'],1), g.w_disspu_ind]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_pel'].is_interp else None
        row = [g.w_pel, [round(self['w_pel'],1), color, False], None, 
               [round(self['w_pel_ind'],1), color, False], g.w_pel_ind]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_dirw'].is_interp else None
        row = [g.w_black, round(self['w_black'],1), None, 
               [round(self['w_dirw'],1), color], g.w_dirw]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_indw'].is_interp else None
        row = [g.w_chip_net, round(self['w_chip_net'],1), None, 
               [round(self['w_indw'],1), color], g.w_indw]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_unkw'].is_interp else None
        row = [g.w_othres_net, round(self['w_othres_net'],1), None, 
               [round(self['w_unkw'],1), color], g.w_unkw]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_pel_net'].is_interp else None
        row = [g.w_pel_net, [round(self['w_pel_net'],1), color, False]]
        writerow(ws,row,r,c)
        r += 1
        color = gray if self['w_pcw'].is_interp else None
        row = [g.w_pcw, [round(self['w_pcw'],1), color, False]]
        writerow(ws,row,r,c)
        r += 1
        row = [g.w_tot_sou, round(self['w_tot_sou'],1), None, 
               round(self['w_tot_use'],1), g.w_tot_use]
        writerow(ws,row,r,c)
        r += 1
        dif = abs(round(self['w_dif'],1))
        if self['w_dif'] < FloatUnit(0,dif.unit):
            dif_lab = 'Unreported sources'
        else:
            dif_lab = 'Unreported uses'
        writerow(ws,[dif],r,c+1)
        writerow(ws,[dif_lab],r+1,c+1)
        
        c = 9
        r = 4
        row = [g.s_pri, round(self['s_pri'],1), str(self.share['s_pri']) if self.share['s_pri'] is not None else None, None, 
               g.s_saw, round(self['s_saw'],1), str(self.share['s_saw']) if self.share['s_saw'] is not None else None]
        writerow(ws,row,r,c)
        r += 1
        row = [g.s_sec, round(self['s_sec'],1), str(self.share['s_sec']) if self.share['s_sec'] is not None else None, None, 
               g.s_pan, round(self['s_pan'],1), str(self.share['s_pan']) if self.share['s_pan'] is not None else None]
        writerow(ws,row,r,c)
        r += 1
        row = [g.s_pcw, round(self['s_pcw'],1), str(self.share['s_pcw']) if self.share['s_pcw'] is not None else None, None, 
               g.s_pu, round(self['s_pu'],1), str(self.share['s_pu']) if self.share['s_pu'] is not None else None]
        writerow(ws,row,r,c)
        r += 4
        s_color = gray if self['s_dirw'].is_interp else None
        u_color = gray if self['s_enetrans'].is_interp else None
        row = [g.s_dirw, [round(self['s_dirw'],1), s_color], [str(self.share['s_dirw']), s_color] if self.share['s_dirw'] is not None else None, None, 
               g.s_enetrans, [round(self['s_enetrans'],1), u_color], [str(self.share['s_enetrans']), u_color] if self.share['s_enetrans'] is not None else None]
        writerow(ws,row,r,c)
        r += 1
        s_color = gray if self['s_indw'].is_interp else None
        u_color = gray if self['s_eneind'].is_interp else None
        row = [g.s_indw, [round(self['s_indw'],1), s_color], [str(self.share['s_indw']), s_color] if self.share['s_indw'] is not None else None, None, 
               g.s_eneind, [round(self['s_eneind'],1), u_color], [str(self.share['s_eneind']), u_color] if self.share['s_eneind'] is not None else None]
        writerow(ws,row,r,c)
        r += 1
        s_color = gray if self['s_unkw'].is_interp else None
        u_color = gray if self['s_eneres'].is_interp else None
        row = [g.s_unkw, [round(self['s_unkw'],1), s_color], [str(self.share['s_unkw']), s_color] if self.share['s_unkw'] is not None else None, None, 
               g.s_eneres, [round(self['s_eneres'],1), u_color], [str(self.share['s_eneres']), u_color] if self.share['s_eneres'] is not None else None]
        writerow(ws,row,r,c)
        r += 1
        u_color = gray if self['s_eneoth'].is_interp else None
        row = [None, None, None, None, 
               g.s_eneoth, [round(self['s_eneoth'],1), u_color], [str(self.share['s_eneoth']), u_color] if self.share['s_eneoth'] is not None else None]
        writerow(ws,row,r,c)
        
        wb.save(filename)
        return

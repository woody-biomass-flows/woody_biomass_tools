# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
"""

# Defines the classes to represent the coefficients and the conversion factors


from base import FloatUnit, FrozenData
from openpyxl import load_workbook


class Coeff(FrozenData):
    """
    interface
    """
    
    __slots__ = ('value',)
    
    __types__ = ('float',)
    
    def __mul__(self, other) -> FloatUnit:
        """
        Valid only for unitless coefficients (e.g. not valid for SWECoeff).

        Parameters
        ----------
        other : FloatUnit
            data to convert.

        Returns
        -------
        FloatUnit
            converted value.

        """
        assert isinstance(other, FloatUnit), "TypeError: must be FloatUnit."
        if other.value is not None:
            return FloatUnit(other.value * self.value,
                             other.unit,
                             other.is_interp)
        else:
            return FloatUnit(None, None, other.is_interp)



class SWECoeff(Coeff):
    """
    Coefficients to SWE of feedstock.
    Only for Material instances.
    """
    
    def __mul__(self, other) -> FloatUnit:
        assert isinstance(other, FloatUnit), "TypeError: must be FloatUnit."
        if other.value is not None:
            assert other.unit[-3:]!='m3SWE', "UnitError: cannot be 'SWE'."
            return FloatUnit(other.value * self.value,
                             'thsm3SWE' if other.unit[:3]=='ths' else 'm3SWE',
                             other.is_interp)
        else:
            return FloatUnit(None, None)

    @classmethod
    def get(cls, settings, info, g, item):
        """
        Get the conversion factor to [m3SWE].

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.
        item : str
            faostat item name.

        Returns
        -------
        SWECoeff
            for the given item.
        """
        
        def apparent_consumption(country, year, item, file_pathname):
            """
            Evaluate the apparent consumption.
            dirty (it was from DB)
        
            Parameters
            ----------
            country : str
                full name of the country.
            year : int
                year.
            item : str
                full item name (as provided in the file).
            file_pathname : str
                file name including the full path.
        
            Returns
            -------
            float
                apparent consumption (production + import - export).
        
            """
            
            el = dict()
        
            wb = load_workbook(file_pathname, read_only=True)
            ws = wb.active
            for row in ws.rows:
                if (row[1].value == country and row[3].value == item and row[7].value == year):
                    try:
                        el[row[5].value] = row[9].value/1.e3
                    except:
                        pass
                if len(el.keys()) == 5:
                    break
            wb.close()
            
            return el.get('production', 0.) +\
                    el.get('import_quantity', 0.) -\
                    el.get('export_quantity', 0.)
    

        if item == g.pel:
            return cls(value=2.346)  # data source: JWEE Conversion Factors
        elif item == g.othagg:
            return cls(value=2.29)  # data source: JWEE Conversion Factors
        elif item == g.recpap:
            # Important: SWE = fiber content of product in this case
            # Weighted average of subclasses coefficients
            val = 0.
            ac = 0.
            tmp = 0.
            ac_tot = 0.
            for item in g.pap_boa:
                ac = apparent_consumption(info.country,
                                          info.year,
                                          item,
                                          settings.jfsq_file_pathname)
                ac_tot += ac
                tmp += g.pap_boa[item] * ac
            val = tmp/ac_tot if ac_tot > 0 else 1.526  # data source: simple average from Jochem et al.
            return cls(value=val)
        elif item == g.recpu:
            # Important: SWE = fiber content of product in this case
            return cls(value=1.7)   # data source: Jochem et al., National wood fiber balances for the pulp and paper sector, 2021
        elif item in g.pap_boa:
            val = g.pap_boa[item]
        else:
            wb = load_workbook(settings.coeff_file_pathname, read_only=True)
            ws = wb.active
            val = 1.0
            for row in ws.rows:
                try:
                    if [r.value for r in row[:3]] == [info.country, g.m_items[item], "Solid Wood Equivalent"]:
                        val = row[3].value
                except:
                    pass
            wb.close()
        return cls(value=float(val))



class SVCoeff(Coeff):
    """
    Coefficients to SWE of product.
    Only for Material instances.
    """
    
    def __mul__(self, other) -> FloatUnit:
        assert isinstance(other, FloatUnit), "TypeError: must be FloatUnit."
        if other.value is not None:
            assert other.unit[-3:]!='m3SWE', "UnitError: cannot be 'SWE'."
            return FloatUnit(other.value * self.value,
                             'thsm3SWE' if other.unit[:3]=='ths' else 'm3SWE',
                             other.is_interp)
        else:
            return FloatUnit(None, None)

    @classmethod
    def get(cls, settings, info, g, item):
        """
        Get the conversion factor to [m3SWE] from DB.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.
        item : str
            faostat item name.

        Returns
        -------
        SVCoeff
            for the given item.
        """
        if item in g.pap_boa:
            #--- Yield ratio for papermaking
            #--- from: "Global Life Cycle Paper Flows, Recycling Metrics, and Material Efficiency", van Ewijk, Stegemann, Ekins (Journal of Industrial Ecology, 22(4), 2018)
            #--- Table I pag. 3
            yr_pap = 0.95
            #---
            val = yr_pap
        elif item == g.recpap:
            yr = 0.893 ## Mantau 2012 from Jochem et al
            val = yr
        else:  
            assortments = [g.out_sawbyp,
                           g.out_othres,
                           g.out_black]
            bypout_coeff_val = 0.
            for ass in assortments:
                bypout_coeff_val += IOCoeff.get(settings, info, item, ass).value
            val = 1. - bypout_coeff_val
        return cls(value=val)



class IOCoeff(Coeff):
    """
    Input / output oefficients.
    Only for Material instances.
    """

    @classmethod
    def get(cls, settings, info, item, assortment):
        """
        Get the input or output coefficient of the given assortment
        for the given faostat item.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        item : str
            faostat item name.
        assortment : str
            Mantau's assortment full name.

        Returns
        -------
        IOCoeff
            input or output coefficient, depending on the assortment full name.
        """
        from globset import m_items
        wb = load_workbook(settings.coeff_file_pathname, read_only=True)
        ws = wb.active
        val = 0.
        for row in ws.rows:
            try:
                if [r.value for r in row[:3]] == [info.country, m_items[item], assortment]:
                    val = float(row[3].value)
                    break
            except:
                pass
        wb.close()
        return cls(value=val)


class Fact(Coeff):
    """
    Conversion factors.
    Only for wood in the rough.
    """
    
    def __init__(self, g, item, sel):
        """
        Build the specific conversion factor.

        Parameters
        ----------
        g : container globset.py
            global variables.
        item : str
            required faostat item name.
        sel : str
            code of the specific conversion factor to output.

        Returns
        -------
        None
        """
        assert sel in ('ub2ob', 'ob2ub','bark'), "Not implemented."
        # data source: "Forest product coversion factors", UN, 2010
        ob2ubcoeff = {g.irwc: 0.88,  
                      g.irwnc: 0.88, 
                      g.fwc: 0.88,
                      g.fwnc: 0.87,
                      g.fw: 0.88}
        if sel == 'ub2ob':
            object.__setattr__(self, 'value', 1./ob2ubcoeff[item])
        elif sel == 'ob2ub':
            object.__setattr__(self, 'value', ob2ubcoeff[item])
        elif sel == 'bark':
            object.__setattr__(self, 'value', 1./ob2ubcoeff[item] - 1.)



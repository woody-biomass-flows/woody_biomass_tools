# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

### Defines the class to manage energy data


from base import FloatUnit, FrozenData
from openpyxl import load_workbook


class EneDataSet(FrozenData):
    """
    Set of energy data for a given country and year.
    """
    
    __slots__ = (
        "direct",
        "indirect",
        "unknown",
        "pcw",
        "transformation",
        "industrial",
        "residential",
        "other",
    )

    @classmethod
    def get(cls, settings, info):
        """
        For a given year and country
        reads energy data from files and returns them as EneDataSet.

        @author: Noemi Cazzaniga - Selene Patani
        @email: noemi.cazzaniga@ext.ec.europa.eu; selene.patani@ext.ec.europa.eu
        @date: May 2023


        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.

        Returns
        -------
        EneDataSet
            energy data.

        """
        input_list = ("primary",
                      "secondary",
                      "pcw",
                      "unknown",
                      "transformation",
                      "industrial",
                      "residential",
                      "other"
                     )
        dic = {k: FloatUnit(None, None) for k in input_list}
        # Read energy sources and uses into a workbook
        wb = load_workbook(settings.ene_file_pathname, read_only=True)
        # Use the active worksheet
        ws = wb.active
        for k in dic:
            # Read one row at a time and save row values corresponding to the country, year and slot selected
            for row in ws.rows:
                if (
                    (row[0].value == info.country)
                    & (row[1].value == info.year)
                    & (row[2].value == k)
                ):
                    # {k: FloatUnit(value, unit, True if interpolated else False)}
                    dic[k] = FloatUnit(row[3].value, row[5].value, bool(row[4].value))
                    break
        wb.close()
        return cls(
            direct=dic["primary"],
            indirect=dic["secondary"] + dic["pcw"],
            unknown=dic["unknown"],
            pcw=dic["pcw"],
            transformation=dic["transformation"],
            industrial=dic["industrial"],
            residential=dic["residential"],
            other=dic["other"],
        )

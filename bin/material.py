# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

### Defines the classes to manage material data


from base import FloatUnit, FrozenData
from coeff import IOCoeff, SWECoeff, SVCoeff, Fact
from openpyxl import load_workbook


class MatDataSet(FrozenData):
    """
    Set of data on materials given a country and year.
    """
    __slots__ = ("data",)

    __types__ = ("dict",)

    @classmethod
    def get(cls, settings, info, g):
        """
        Get the data.

        Parameters
        ----------
        dataset : dict
             set of Material instances.

        Returns
        -------
        MatDataSet

        """
        dataset = Material.get_all(settings, info, g)
        return cls(data=dataset)


class Material(FrozenData):
    """
    Set of data on materials given item, country and year.
    """
    __slots__ = ("item", "production", "export_quantity", "import_quantity")

    __types__ = (
        "str",
        "FloatUnit",
        "FloatUnit",
        "FloatUnit",
    )

    @property
    def net_import(self) -> FloatUnit:
        return self.import_quantity - self.export_quantity

    def io_qty(self, settings, info, assortment) -> FloatUnit:
        """
        Gives the input or output quantity related to the production.
        Applicable only to quantity given in [m3SWE].

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        assortment : str
            assortment name (see globset).

        Returns
        -------
        FloatUnit
        """
        f = IOCoeff.get(settings, info, self.item, assortment)
        return f * self.production

    def toswe(self, settings, info, g):
        """
        Convert to [m3SWE].

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.

        Returns
        -------
        Material
            extra attributes are lost (e.g. Faostat)
        """
        f = SWECoeff.get(settings, info, g, self.item)
        return Material(
            item=self.item,
            production=f * self.production,
            export_quantity=f * self.export_quantity,
            import_quantity=f * self.import_quantity,
        )

    def tosv(self, settings, info, g):
        """
        Convert from [m3SWE] of feedstock (input assortment)
        to [m3SWE] of product.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.

        Returns
        -------
        Material

        """
        f = SVCoeff.get(settings, info, g, self.item)
        return Material(
            item=self.item,
            production=f * self.production,
            export_quantity=f * self.export_quantity,
            import_quantity=f * self.import_quantity,
        )

    def to(self, descr, g):
        """
        Convert according to description.

        Parameters
        ----------
        descr : 'str'
            can be:
                'ub2ob': convert to [m3 o.b.]
                'ob2ub': convert to [u.b.]
        g : container globset.py
            global variables.

        Returns
        -------
        Material
            extra attributes are lost (e.g. Faostat)
        """
        f = Fact(g, self.item, descr)
        return Material(
            item=self.item,
            production=f * self.production,
            export_quantity=f * self.export_quantity,
            import_quantity=f * self.import_quantity,
        )

    def bark(self, g):
        """
        Evaluates the bark from removals and trade.
        Applicable only to Roundwood u.b. (check not implemented).
        """
        f = Fact(g, self.item, "bark")
        return Material(
            item=self.item,
            production=f * self.production,
            export_quantity=f * self.export_quantity,
            import_quantity=f * self.import_quantity,
        )

    @classmethod
    def get(cls, settings, info, g, item):
        """
        Get the data for an item.

        @author: Noemi Cazzaniga - Selene Patani
        @email: noemi.cazzaniga@ext.ec.europa.eu; selene.patani@ext.ec.europa.eu
        @date: May 2023

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.
        item : str
            wanted JFSQ item (full name).

        Returns
        -------
        Material

        """
        dic = {k: FloatUnit(None, None) for k in cls.__slots__}
        dic["item"] = item if item != g.osb else g.partboa
        # Read forestry data into a workbook
        wb = load_workbook(settings.jfsq_file_pathname, read_only=True)
        # Use the active worksheet
        ws = wb.active
        for k in cls.__slots__[1:]:
            for row in ws.rows:
                # Read one row at a time and save row values corresponding to the country, year and element of the item selected
                if (
                    (row[1].value == info.country)
                    & (row[3].value == item)
                    & (row[5].value == k)
                    & (row[7].value == info.year)
                ):
                    # {k: FloatUnit(value, unit, True if interpolated else False)}
                    dic[k] = FloatUnit(row[9].value/1.e3, 'ths'+row[8].value, bool(row[11].value))
                    break
        # Close the excel
        wb.close()
        mat = cls(dic)
        return mat.toswe(settings, info, g)

    @staticmethod
    def get_all(settings, info, g) -> dict:
        """
        Get the data for all the items.
        it gives the result in m3SWE

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.

        Returns
        -------
        dict
            for each item in globset.item_list a Material instance.
        """
        d = {}
        for i in g.item_list:
            d[i] = Material.get(settings, info, g, i)
        return d

# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

### Defines standard classes for the modules


from base import Data, FrozenData
from material import MatDataSet
from energy import EneDataSet



class Info(Data):
    """
    Data of the specific case to evaluate
    mutable
    can be filled partially, but repr doesn't work with partially filled class
    """
    __slots__ = ('country',
                 'year',
                 )

    __types__ = ('str',
                 'int',
                 )

    @classmethod
    def build(cls, settings, g, country, year):
        """
        Build a complete Info instance.

        Parameters
        ----------
        settings : Settings
            see std.
        g : container globset.py
            global variables.
        country : str
            full official name (see globset).
        year : int
            year.

        Returns
        -------
        Info
            with all the attributes instantiated.

        """
        return cls({'country': country,
                    'year' : year,
                   })



class Settings(FrozenData):
    """
    Defines the settings for the run
    """

    __slots__ = ('jfsq_file_pathname',
                 'ene_file_pathname',
                 'coeff_file_pathname',
                 'outfile_path',
                 'unit',
                 )

    __types__ = ('str',
                 'str',
                 'str',
                 'str',
                 'str'
                 )



class DataSet(FrozenData):
    """
    Stores all the data required for the next calculus steps.
    """

    __slots__ = ('material',
                 'energy',
                 )

    __types__ = ('MatDataSet',
                 'EneDataSet',
                 )

    @classmethod
    def get(cls, settings, info, g):
        """
        Read the whole dataset required for calculus of one country and one year.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.

        Returns
        -------
        DataSet
        """
        mat_data = MatDataSet.get(settings, info, g)
        ene_data = EneDataSet.get(settings, info)
        return cls(material=mat_data, 
                   energy=ene_data)

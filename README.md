# woody_biomass_tools

Python tools to evaluate Wood Resource Balance and Sankey diagram 
according to the approaches described in
[Cazzaniga et al (2021)][wrbref] for the Wood Resource Balance (WRB)
and [...][sankeyref] for the Sankey diagram.

Requires Python 3.6+

Requires openpyxl

## How to run it
In `main_sankey.py` and `main_wrb.py` replace the paths and names of the input and output files:

    jfsq_file_pathname = os.path.join(".","forestry_data.xlsx")
    ene_file_pathname = os.path.join(".","wood_energy_sources_uses.xlsx")
    coeff_file_pathname = os.path.join(".","input_output_coefficients.xlsx")
    outfile_path = "."

Initialize the information regarding country and  year, in the example `Austria` and year `2015`:

    info = Info.build(settings, g, "Austria", 2015)

> N.B.: Results are provided in 1000 m³ SWE (variable `unit` in `Settings`).

# Provided files:

## Modules
* base.py
* coeff.py
* energy.py
* globset.py
* main_sankey.py
* main_wrb.py
* material.py
* sankey.py
* std.py
* wrb.py

## Example of input data
* forestry_data.xlsx
* input_output_coefficients.xlsx
* wood_energy_sources_uses.xlsx

## Example of output
* Forestry_Sankey_2015.xlsx
* WRB_2015.xlsx

## Ancillary files
* arrow_codes.xlsx
* Definitions.txt


# Description of files:

## base.py
This module defines the basic classes used in the modules:
* FloatUnit
* Data
* FrozenData

## coeff.py
It defines the classes to represent the coefficients and the conversion factors:
* Coeff
* SWECoeff
* SVCoeff
* IOCoeff
* Fact

## energy.py
This module defines the EneDataSet class to manage energy data.

## globset.py
Here variables that are used in many modules are initialized.

## main_sankey.py
Example of main code to evaluate one Sankey diagram. It uses the provided example data.

## main_wrb.py
Example of main code to evaluate one WRB. It uses the provided example data.

## material.py
This module defines the classes to manage data of wood in the rough, and material production and trade:
* MatDataSet
* Material

## sankey.py
It defines the classes to evaluate the data of a Sankey diagram:
* SankeyCore
* DetailedSankey

## std.py
It defines standard classes for the modules:
* Info
* Settings
* DataSet

## wrb.py
It defines the class Wrb, needed to evaluate the data of a WRB.

## forestry_data.xlsx
Example of input forestry data. It contains the following columns, in the following order:
* Area Code [int]: numerical code of the country
* Area [str]: full country name
* Item Code [int]: numerical code of the item
* Item [str]: full name of the item according to JFSQ nomenclature (see globset.py)
* Element Code [int]: numerical code of the element
* Element [str]: full name of the item according to JFSQ nomenclature (see globset.py)
* Year Code [int]: year
* Year [int]: year
* Unit [str]: unit of the data, can be "m3" or "tonnes"
* Value [float]: data
* Flag [str]: flag representing the data quality (not used by the code)
* Interpolated [int]: 0=original data, 1=interpolated value

## input_output_coefficients.xlsx
Example of file containing both input/output coefficients for the material sectors and conversion factors to SWE of feedstock to industry.
The columns are in the following order:
* Country [str]: full country name
* Product [str]: full name of the product according to Mantau's terminology (see globset.py)
* Assortment [str]: full name of the input/output assortment according to Mantau's terminology (see globset.py)
* Coefficient [float]: value of the coefficient

## wood_energy_sources_uses.xlsx
Example of input energy data, organised in the following columns:
* Country [str]: full country name
* Year [int]: year
* Source/Use [str]: description field, can be: "direct", "indirect", "pcw", "unknown", "transformation", "industrial", "residential", "other"
* Value [float]: data
* Interpolated [int]: 0=original data, 1=interpolated value
* Unit [str]: unit of the data, must be "thsm3SWE"

## Forestry_Sankey_2015.xlsx
Example of output file of a Sankey diagram, obtained using the provided example input files.

## WRB_2015.xlsx
Example of output file of a WRB, obtained using the provided example input files.

## arrow_codes.xlsx
Description of the arrows representing the flows in the Sankey diagram:
* code: internal code of the arrow (used in the Python modules)
* start_node (label as used for the visualisation of the Sankey diagram)
* end node (label as used for the visualisation of the Sankey diagram)
* biomass_type: type of biomass included in the flow

## Definitions.txt
Definitions of all the items, both for the WRB and the Sankey diagram.

# Disclaimer:
This code was written for producing the results published in 
[Cazzaniga et al (2021)][wrbref]
and [...][sankeyref],
and is provided "as is", without any warranty on its validity
with different data input.


[wrbref]: https://publications.jrc.ec.europa.eu/repository/handle/JRC126552
[sankeyref]: ...
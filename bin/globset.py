# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

#### Sets parameters used in different parts of the code


### MS where to use JWEE data for energy
# not used
jwee_list = [ "Austria",
              "Croatia",
              "Cyprus",
              "Czechia",
              "Estonia",
              "Finland",
              "France",
              "Germany",
              "Ireland",
              "Italy",
              "Lithuania",
              "Luxembourg",
              "Netherlands",
              "Slovakia",
              "Slovenia",
              "Sweden",
              "United Kingdom"
             ]


### Parameters for processing:

### JFSQ full names
irwc = 'Industrial roundwood, coniferous'
irwnc = 'Industrial roundwood, non-coniferous'
fwc = 'Wood fuel, coniferous'
fwnc = 'Wood fuel, non-coniferous'
fw = 'Wood fuel'
sawc = 'Sawnwood, coniferous'
sawnc = 'Sawnwood, non-coniferous all'
ply = 'Plywood'
venshe = 'Veneer sheets'
fibboa = 'Fibreboard'
partboa = 'Particle board'    # to merge with osb
osb = 'OSB'    # to merge with particle board
chempu = 'Chemical wood pulp'
semichempu = 'Semi-chemical wood pulp'
mechpu = 'Mechanical wood pulp'
disspu = 'Dissolving wood pulp'
# pap = 'Paper and paperboard'
cartboa ='Cartonboard'
casemat = 'Case materials'
sanipa = 'Household and sanitary papers'
newsp = 'Newsprint'
othpapnes = 'Other paper and paperboard n.e.s. (not elsewhere specified)'
othpack = 'Other papers mainly for packaging'
pripamech ='Printing and writing papers, uncoated, mechanical'
pripafree ='Printing and writing papers, uncoated, wood free'
pripacoat ='Printing and writing papers, coated'
wrappa = 'Wrapping papers'
recpap ='Recovered paper'
recpu = 'Recovered fibre pulp'
chip = 'Wood chips and particles'
res = 'Wood residues'
pel = 'Wood pellets'    # to merge with other agglomerates
othagg = 'Other agglomerates'    # to merge with pellets

item_list = [irwc,
             irwnc, 
             fwc,
             fwnc,
             fw,
             osb,
             sawc,
             sawnc,
             ply,
             venshe,
             fibboa,
             partboa,
             chempu,
             semichempu,
             mechpu,
             disspu,
             cartboa,
             casemat,
             sanipa,
             newsp,
             othpapnes,
             othpack,
             pripamech,
             pripafree,
             pripacoat,
             wrappa,
             recpap,
             recpu,
             chip,
             res,
             pel,
             othagg,
            ]

### Conversion factors to swe for the paper sector
# data source: Jochem et al., National wood fiber balances for the pulp and paper sector, 2021
pap_boa = {cartboa: 1.726,
           casemat: 1.747,
           sanipa: 1.895,
           newsp: 1.653,
           othpapnes: 1.453,
           othpack: 1.642,
           pripamech: 1.368,
           pripafree: 1.474,
           pripacoat: 1.211,
           wrappa: 1.895}


### Mantau's assortments:
in_wirc = "Wood in the rough (C) u. b."
in_wirnc = "Wood in the rough (NC) u. b."
in_forres = "Forest residues"
in_bark = "Bark"
in_sawbyp = "Sawmill by products"
in_othres = "Other industrial residues"
in_pcw = "Post consumer wood"
out_bark = "Output Bark"
out_sawbyp = "Output Sawmill by products"
out_othres = "Output Other industrial residues"
out_black = "Output Black liquor"

### Mantau's products:
m_items = {
    sawc: "Sawnwood (C)",
    sawnc: "Sawnwood (NC)",
    fibboa: "Fibreboard + (Total)",
    partboa: "Particle Board",
    ply: "Plywood",
    venshe: "Veneer Sheets",
    mechpu: "Mechanical Wood Pulp",
    semichempu: "Semi-Chemical Wood Pulp",
    chempu: "Chemical Wood Pulp",
    disspu: "Dissolving Wood Pulp"
    }


### WRB Labels:
## WRB main_table:
# Primary:
w_irwc_rem = 'Industrial roundwood removals (conifer)'
w_irwnc_rem = 'Industrial roundwood removals (non-conifer)'
w_fwc_rem = 'Fuel wood removals (conifer)'
w_fwnc_rem = 'Fuel wood removals (non-conifer)'
w_irwc_net = 'Net-import industrial roundwood (conifer)'
w_irwnc_net = 'Net-import industrial roundwood (non-conifer)'
w_fw_net = 'Net-import fuel wood'
w_bark = 'Bark'

# Secondary:
w_sawres = 'Sawmill residues'
w_othres = 'Other industrial residues'
w_pel = 'Wood pellets'
w_black = 'Black liquor'
w_chip_net = 'Net-import wood chips and particles'
w_othres_net = 'Net-import other wood residues'
w_pel_net = 'Net-import wood pellets'

# pcw:
w_pcw = 'Post-consumer wood'

# Material:
w_sawc_ind = 'Sawmill industry (conifer)'
w_sawnc_ind = 'Sawmill industry (non-conifer)'
w_venshe_ind = 'Veneer sheets industry'
w_ply_ind = 'Plywood industry'
w_partboa_ind = 'Particle board industry'
w_fibboa_ind = 'Fiberboard industry'
w_mechpu_ind = 'Mechanical pulp industry'
w_chempu_ind = 'Chemical pulp industry'
w_semichempu_ind = 'Semi-chemical pulp industry'
w_disspu_ind = 'Dissolving pulp industry'
w_pel_ind = 'Wood pellets industry'

# Energy:
w_dirw = 'Direct wood'
w_indw = 'Indirect wood'
w_unkw = 'Unknown wood'

# Footer:
w_tot_sou = 'Total sources'
w_tot_use = 'Total uses'
w_dif = 'Difference'

## Summary tables (with shares)
s_pri = 'Primary wood'
s_sec = 'Secondary woody biomass'
s_pcw = 'Post-consumer wood'
s_saw = 'Sawmill industry'
s_pan = 'Panel industry'
s_pu = 'Wood pulp industry'
s_dirw = 'Direct wood'
s_indw = 'Indirect wood'
s_unkw = 'Unknown wood'
s_enetrans = 'Energy transformation'
s_eneind = 'Industrial internal consumption'
s_eneres = 'Residential sector'
s_eneoth = 'Other'

# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
"""

### Defines the basic classes used in the modules


from abc import ABC



class FloatUnit:
    """
	Base class to represent an amount of biomass
    __mul__ is not defined because of the unit
	__slots__ instead of __dict__
    """
    
    __slots__ = ('value',     # float or None
                 'unit',      # str or None
                 'is_interp'  # bool
				 )

    # basic methods

    def __init__(self, value, unit, is_interp = False):
        try:
            v = float(value)
        except:
            v = None
        object.__setattr__(self, 'value', v)
        object.__setattr__(self, 'unit', unit)
        object.__setattr__(self, 'is_interp', is_interp)
    
    def __setattr__(self, *args):
        return NotImplemented
    
    def __delattr__(self, *arg):
        return NotImplemented

    def __eq__(self, other):
        try:
            eq = (type(self) == type(other) and self.value == other.value and self.unit == other.unit)
        except:
            eq = False
        return eq

    def __format__(self, form):
        """
        Parameters
        ----------
        form : format using Python Mini-Language.
        The boolean in the end represents is_interp attribute.

        Returns
        -------
        str
        
        Example
        -------
        form = '.2f'
        output -> '3.00 m True'
        """
        return ('{0.value:' + form + '} {0.unit}  {0.is_interp}').format(self)

    def __ge__(self, other):
        try:
            ge = (type(self) == type(other) and self.value >= other.value and self.unit == other.unit)
        except:
            ge = False
        return ge

    def __gt__(self, other):
        try:
            gt = (type(self) == type(other) and self.value > other.value and self.unit == other.unit)
        except:
            gt = False
        return gt

    def __hash__(self):
        return hash(tuple(self.value, self.unit))

    def __le__(self, other):
        try:
            le = (type(self) == type(other) and self.value <= other.value and self.unit == other.unit)
        except:
            le = False
        return le

    def __lt__(self, other):
        try:
            lt = (type(self) == type(other) and self.value < other.value and self.unit == other.unit)
        except:
            lt = False
        return lt

    def __ne__(self, other):
        try:
            ne = (type(self) != type(other) or self.value != other.value or self.unit != other.unit)
        except:
            ne = False
        return ne

    def __repr__(self):
        return ('{0.value} {0.unit} {0.is_interp} {1}').format(self, type(self))

    def __sizeof__(self):
        return NotImplemented
    
    def __str__(self):
        if self.value is None:
            return '-'
        return ('{0.value}{0.unit}').format(round(self,1))

    def __abs__(self):
        if self.value is None:
            a = None
        else:
            a = abs(self.value)
        return FloatUnit(a, self.unit, self.is_interp)

    def __add__(self, other):
        if type(self) == type(other):
            if self.value is None:
                if other.value is None:
                    return FloatUnit(None, self.unit or other.unit, self.is_interp)
                elif (self.unit is None) or (self.unit == other.unit):
                    return other
                else:
                    raise Exception("AddError: FloatUnits must have the same units. {0} + {1}".format(self.unit, other.unit))
            elif other.value is None:
                if (other.unit is None) or (self.unit == other.unit):
                    return self
                else:
                    raise Exception("AddError: FloatUnits must have the same units. {0} + {1}".format(self.unit, other.unit))
            else:
                if self.unit == other.unit:
                    return FloatUnit(self.value + other.value, self.unit, (self.is_interp or other.is_interp))
                else:
                    raise Exception("AddError: FloatUnits must have the same units. {0} + {1}".format(self.unit, other.unit))
        else:
            return NotImplemented

    def __neg__(self):
        return FloatUnit(-self.value, self.unit, self.is_interp)

    def __pos__(self):
        return FloatUnit(self.value, self.unit, self.is_interp)

    def __round__(self, *arg):
        if len(arg) > 1:
            return NotImplemented
        if self.value is None:
            return self
        if len(arg) == 1:
            return FloatUnit(round(self.value, arg[0]), self.unit, self.is_interp)
        else:
            return FloatUnit(round(self.value), self.unit, self.is_interp)

    def __sub__(self, other):
        if type(self) == type(other):
            if self.value is None:
                if other.value is None:
                    return FloatUnit(None, self.unit or other.unit, self.is_interp)
                elif (self.unit is None) or (self.unit == other.unit):
                    return FloatUnit(-other.value, other.unit, other.is_interp)
                else:
                    raise Exception("SubError: FloatUnits must have the same units. {0} - {1}".format(self.unit,other.unit))
            elif other.value is None:
                return FloatUnit(self.value, self.unit, self.is_interp)
            else:
                if self.unit == other.unit:
                    return FloatUnit(self.value - other.value, self.unit, (self.is_interp or other.is_interp))
                else:
                    raise Exception("SubError: FloatUnits must have the same units. {0} - {1}".format(self.unit,other.unit))
        else:
            return NotImplemented
    
    def is_zero_or_null(self):
        """
        Check if the value is 0. or None

        Returns
        -------
        boolean
            True if value is zero or None.
        """
        return (self.value == 0. or self.value is None)
    
    def share(self, other):
        """
        Proportion between two FloatUnits.
        It is in %.
        Neglects is_interp.

        Parameters
        ----------
        other : FloatUnit
            denominator.

        Returns
        -------
        FloatUnit
            percentage (self / other * 100).

        """
        assert(isinstance(other, FloatUnit)), "TypeError: must be FloatUnit."
        if self.value == 0 and other.value == 0:
            return FloatUnit(0., '%')
        elif (self.value is None) or (other.value is None):
            return FloatUnit(None, None)
        else:
            return FloatUnit(self.value / other.value * 100., '%')



class Data(ABC):
    """
	Base class to represent a single data
    hashable class
    __slots__ instead of __dict__
    __delattr__ not implemented
    Default type: FloatUnit
    """
    
    def __init__(self, dic = None, **kwargs):
        if dic and kwargs:
            raise Exception("Too many arguments.")
        if kwargs:
            dic = kwargs
        if any(k not in self.__slots__ for k in dic):
            raise Exception("Wrong argument name. Names should be in {n}:".format(n = self.__slots__))
        try: # if self.__types__
            if all(type(dic[k]).__name__ == self.__types__[self.__slots__.index(k)] for k in dic):
                for k in dic:
                    object.__setattr__(self, k, dic[k])
            else:
                print(tuple(type(i[1]).__name__ for i in dic.items()))
                raise Exception("Wrong argument type.")
        except:
            if all(isinstance(i[1], FloatUnit) for i in dic.items()):
                for k in dic.keys():
                    object.__setattr__(self, k, dic[k])
            else:
                raise Exception("Wrong argument type. It must be FloatUnit.")
    
    def __hash__(self):
        return hash(self.__repr__())
    
    def __eq__(self, other):
        try:
            return (type(self) == type(other) and 
                    self.__slots__ == other.__slots__ and 
                    all(self.__getattribute__(a) == other.__getattribute__(a) for a in self.__slots__))
        except:
            return False
    
    def __repr__(self):
        return '{0}{1}'.format(type(self),
                               dict(zip(self.__slots__,
                                     tuple(str(self.__getattribute__(s)) for s in self.__slots__))
                                 ))

    def __str__(self):
        return '{0!r}'.format(self)
    
    def __delattr__(self, attr):
        raise Exception("Attribute '{attr}' cannot be deleted.".format(attr=attr))
        
    def __setattr__(self, name, value):
        if name not in self.__slots__:
            raise Exception("Attribute '{attr}' cannot be set.".format(attr=name))
        else:
            return object.__setattr__(self, name, value)
    
    def isnull(self):
        check = [self.__getattribute__(i) is None for i in self.__slots__]
        return (all(check))

    def hasnull(self):
        check = [self.__getattribute__(i) is None for i in self.__slots__]
        return (any(check))

    @classmethod
    def get(cls,*args):
        pass



class FrozenData(Data):
    """
	Base frozen class to represent a single data
    hashable class
    __slots__ instead of __dict__
    __setattr__ and __delattr__ not implemented
    """
    def __init__(self, dic = None, **kwargs):
        if dic and kwargs:
            raise Exception("Too many arguments.")
        if kwargs:
            dic = kwargs
        if set(dic.keys()) != set(self.__slots__):
            raise Exception("Wrong argument name. Names should be:".format(self.__slots__))
        try: # if self.__types__
            if all(type(dic[k]).__name__ == self.__types__[self.__slots__.index(k)] for k in dic):
                for k in dic:
                    object.__setattr__(self, k, dic[k])
            else:
                print(tuple(type(i[1]).__name__ for i in dic.items()))
                raise Exception("Wrong argument type.")
        except:
            if all(isinstance(i[1], FloatUnit) for i in dic.items()):
                for k in dic.keys():
                    object.__setattr__(self, k, dic[k])
            else:
                raise Exception("Wrong argument type. It must be FloatUnit.")
   
    def __setattr__(self, name, value):
        raise Exception("Attribute '{attr}' cannot be set.".format(attr=name))

# -*- coding: utf-8 -*-
"""
@author: Noemi Cazzaniga
@email: noemi.cazzaniga@ext.ec.europa.eu
@date: Sep 2021
@revision: May 2023
"""

### Defines the classes to evaluate the data of a Sankey diagram
### see: https://knowledge4policy.ec.europa.eu/visualisation/interactive-sankey-diagrams-woody-biomass-flows-eu-member-states_en


import openpyxl as xl
from base import FloatUnit
from coeff import Coeff
import os


class SankeyCore(dict):
    """
    Core intermediate results to evaluate the data of a Sankey diagram
    to be stacked for aggregated diagrams
    mutable
    """
    
    def __add__(self, other):
        """
        Cumulate Sankey core values item by item.

        Parameters
        ----------
        other : Sankey.

        Returns
        -------
        new : Sankey.

        """
        assert(type(self) == type(other)), "TypeError."
        new = SankeyCore()
        for k in other.keys():
            if k in self.keys():
                new[k] = self[k] + other[k]
            else:
                new[k] = other[k]
        return new
    
    @classmethod
    def calc(cls, settings, info, g, data):
        """
        Evaluate the Sankey core values (overbark). NO nodes and arrows.

        Parameters
        ----------
        settings : Settings
            see std.
        info : Info
            see std.
        g : container globset.py
            global variables.
        data : DataSet
            all input data.

        Returns
        -------
        s : SankeyCore
            all Sankey core values
            incl. ancillary values.
            All values are FloatUnit

        """
        s = cls()
        
        # pcw node
        s['pcw'] = data.material.data[g.partboa].io_qty(settings, info, g.in_pcw) +\
                     data.material.data[g.osb].io_qty(settings, info, g.in_pcw) +\
                     data.energy.pcw
                     
        # saw_ind node
        sawc_ind = data.material.data[g.sawc].production
        sawnc_ind = data.material.data[g.sawnc].production
        s['saw_ind'] = sawc_ind +\
                        sawnc_ind

        # pan_ind node (incl. bark)
        plyven_ind = data.material.data[g.venshe].production +\
                        data.material.data[g.ply].production
        partboa_ind = data.material.data[g.partboa].production +\
                         data.material.data[g.osb].production
        fibboa_ind = data.material.data[g.fibboa].production
        s['pan_ind'] =  plyven_ind +\
                           	partboa_ind +\
                       	    fibboa_ind

        # pulp_ind node
        pu4pa_ind = data.material.data[g.mechpu].production +\
                           data.material.data[g.chempu].production +\
                           data.material.data[g.semichempu].production
        disspu_ind = data.material.data[g.disspu].production
        s['pu_ind'] = pu4pa_ind +\
            	       disspu_ind

        # saw_ind2byprod arrow
        s['saw_ind2byprod'] = data.material.data[g.sawc].io_qty(settings, info, g.out_sawbyp) +\
                            data.material.data[g.sawnc].io_qty(settings, info, g.out_sawbyp)
        # pan_ind2byprod arrow
        out_plyven = data.material.data[g.venshe].io_qty(settings, info, g.out_othres) +\
                       data.material.data[g.ply].io_qty(settings, info, g.out_othres)
        out_partboa = data.material.data[g.partboa].io_qty(settings, info, g.out_othres) +\
                         data.material.data[g.osb].io_qty(settings, info, g.out_othres)
        out_fibboa = data.material.data[g.fibboa].io_qty(settings, info, g.out_othres)
        s['pan_ind2byprod'] = out_plyven +\
                               out_partboa +\
                               out_fibboa

        # pulp_ind2byprod arrow
        out_pu4pa = data.material.data[g.mechpu].io_qty(settings, info, g.out_black) +\
                        data.material.data[g.chempu].io_qty(settings, info, g.out_black) +\
                        data.material.data[g.semichempu].io_qty(settings, info, g.out_black)
        out_disspu = data.material.data[g.disspu].io_qty(settings, info, g.out_black)
        s['pu_ind2byprod'] = out_pu4pa +\
                                 out_disspu 
        # pel_ind node
        s['pel_ind'] = data.material.data[g.pel].production +\
                        data.material.data[g.othagg].production

        # ene_tot2pel_ind arrow
        s['ene_tot2pel_ind'] = s['pel_ind']
                        
        # ene_tot node
        s['_dir'] = data.energy.direct
        s['_ind'] = data.energy.indirect
        s['_unk'] = data.energy.unknown
        s['_energy'] = s['_dir'] +\
                        s['_ind'] +\
                        s['_unk']

        # pcw2pan_ind arrow
        s['pcw2pan_ind'] = data.material.data[g.partboa].io_qty(settings, info, g.in_pcw) +\
                              data.material.data[g.osb].io_qty(settings, info, g.in_pcw)
        
        # pcw2ene_tot arrow
        s['pcw2ene_tot'] = data.energy.pcw
        
        # rw_rem node (overbark)
        s['rw_rem'] = data.material.data[g.irwc].to('ub2ob',g).production +\
                        data.material.data[g.irwnc].to('ub2ob',g).production +\
                        data.material.data[g.fwc].to('ub2ob',g).production +\
                        data.material.data[g.fwnc].to('ub2ob',g).production
                        
        # rw_trade nodes (overbark)
        s['_rw_imp'] = data.material.data[g.irwc].to('ub2ob',g).import_quantity +\
                        data.material.data[g.irwnc].to('ub2ob',g).import_quantity +\
                        data.material.data[g.fw].to('ub2ob',g).import_quantity
        s['_rw_exp'] = data.material.data[g.irwc].to('ub2ob',g).export_quantity +\
                        data.material.data[g.irwnc].to('ub2ob',g).export_quantity +\
                        data.material.data[g.fw].to('ub2ob',g).export_quantity
        
        # bark info
        bark4pan = data.material.data[g.osb].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.ply].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.venshe].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.fibboa].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.partboa].io_qty(settings, info, g.in_bark)
        bark4pu = data.material.data[g.chempu].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.semichempu].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.mechpu].io_qty(settings, info, g.in_bark) +\
                    data.material.data[g.disspu].io_qty(settings, info, g.in_bark)
        bark_irwc_mat = data.material.data[g.irwc].bark(g) 
        bark_irwnc_mat = data.material.data[g.irwnc].bark(g) 
        bark_fwc_mat = data.material.data[g.fwc].bark(g) 
        bark_fwnc_mat = data.material.data[g.fwnc].bark(g) 
        bark_fw_mat = data.material.data[g.fw].bark(g)
        bark_tot = bark_irwc_mat.production +\
                    bark_irwnc_mat.production +\
                    bark_fwc_mat.production +\
                    bark_fwnc_mat.production +\
                    bark_irwc_mat.net_import +\
                    bark_irwnc_mat.net_import +\
                    bark_fw_mat.net_import
        s['_bark_tot'] = bark_tot
        s['_bark4pan'] = bark4pan
        s['_bark4pu'] = bark4pu
        
        # rw4mat2saw_ind arrow
        s['rw4mat2saw_ind'] = data.material.data[g.sawc].io_qty(settings, info, g.in_wirc) +\
                               data.material.data[g.sawnc].io_qty(settings, info, g.in_wirnc)
        
        # rw4mat2pan_ind arrow (incl. bark4mat)
        s['rw4mat2pan_ind'] = data.material.data[g.osb].io_qty(settings, info, g.in_wirc) +\
                                  data.material.data[g.osb].io_qty(settings, info, g.in_wirnc) +\
                                  data.material.data[g.osb].io_qty(settings, info, g.in_forres) +\
                                  data.material.data[g.ply].io_qty(settings, info, g.in_wirc) +\
                                  data.material.data[g.ply].io_qty(settings, info, g.in_wirnc) +\
                                  data.material.data[g.ply].io_qty(settings, info, g.in_forres) +\
                                  data.material.data[g.venshe].io_qty(settings, info, g.in_wirc) +\
                                  data.material.data[g.venshe].io_qty(settings, info, g.in_wirnc) +\
                                  data.material.data[g.venshe].io_qty(settings, info, g.in_forres) +\
                                  data.material.data[g.fibboa].io_qty(settings, info, g.in_wirc) +\
                                  data.material.data[g.fibboa].io_qty(settings, info, g.in_wirnc) +\
                                  data.material.data[g.fibboa].io_qty(settings, info, g.in_forres) +\
                                  data.material.data[g.partboa].io_qty(settings, info, g.in_wirc) +\
                                  data.material.data[g.partboa].io_qty(settings, info, g.in_wirnc) +\
                                  data.material.data[g.partboa].io_qty(settings, info, g.in_forres) +\
                                  bark4pan
        
        # rw4mat2pu_ind arrow (incl. bark4mat)
        s['rw4mat2pu_ind'] = data.material.data[g.chempu].io_qty(settings, info, g.in_wirc) +\
                                data.material.data[g.chempu].io_qty(settings, info, g.in_wirnc) +\
                                data.material.data[g.chempu].io_qty(settings, info, g.in_forres) +\
                                data.material.data[g.semichempu].io_qty(settings, info, g.in_wirc) +\
                                data.material.data[g.semichempu].io_qty(settings, info, g.in_wirnc) +\
                                data.material.data[g.semichempu].io_qty(settings, info, g.in_forres) +\
                                data.material.data[g.mechpu].io_qty(settings, info, g.in_wirc) +\
                                data.material.data[g.mechpu].io_qty(settings, info, g.in_wirnc) +\
                                data.material.data[g.mechpu].io_qty(settings, info, g.in_forres) +\
                                data.material.data[g.disspu].io_qty(settings, info, g.in_wirc) +\
                                data.material.data[g.disspu].io_qty(settings, info, g.in_wirnc) +\
                                data.material.data[g.disspu].io_qty(settings, info, g.in_forres) +\
                                bark4pu
        
        # rw4mat node (incl. bark4mat)
        s['rw4mat'] = s['rw4mat2saw_ind'] + s['rw4mat2pan_ind'] + s['rw4mat2pu_ind']
        
        # rw_tot2rw4mat node (incl. bark4mat)
        s['rw_tot2rw4mat'] = s['rw4mat']
        
        # byprod_trade nodes
        s['_byprod_imp'] = data.material.data[g.chip].import_quantity +\
                              data.material.data[g.res].import_quantity
        s['_byprod_exp'] = data.material.data[g.chip].export_quantity +\
                              data.material.data[g.res].export_quantity

        # pel_trade nodes
        s['_pel_imp'] = data.material.data[g.pel].import_quantity +\
                         data.material.data[g.othagg].import_quantity
        s['_pel_exp'] = data.material.data[g.pel].export_quantity +\
                         data.material.data[g.othagg].export_quantity
        
        # byprod2pan_ind arrow
        s['byprod2pan_ind'] = data.material.data[g.osb].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.osb].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.ply].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.ply].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.venshe].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.venshe].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.fibboa].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.fibboa].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.partboa].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.partboa].io_qty(settings, info, g.in_othres)

        # byprod2pu_ind arrow
        s['byprod2pu_ind'] = data.material.data[g.chempu].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.chempu].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.semichempu].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.semichempu].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.mechpu].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.mechpu].io_qty(settings, info, g.in_othres) +\
                                data.material.data[g.disspu].io_qty(settings, info, g.in_sawbyp) +\
                                data.material.data[g.disspu].io_qty(settings, info, g.in_othres)

        # sawnw_dom
        s['_sawnw_dom'] = s['saw_ind'] - s['saw_ind2byprod'] 
        
        # saw_ind2sawnw arrow
        s['saw_ind2sawnw'] = s['_sawnw_dom']
      
        # plyven_dom
        s['_plyven_dom'] = plyven_ind - out_plyven
        
        # pan_ind2plyven arrow
        s['pan_ind2plyven'] = s['_plyven_dom']

        # partboa_dom
        s['_partboa_dom'] = partboa_ind - out_partboa
        
        # pan_ind2partboa arrow
        s['pan_ind2partboa'] = s['_partboa_dom']

        # fibboa_dom
        s['_fibboa_dom'] = fibboa_ind - out_fibboa
        
        # pan_ind2fibboa arrow
        s['pan_ind2fibboa'] = s['_fibboa_dom']

        # pu_ind2pu4pa arrow
        s['pu_ind2pu4pa'] =  pu4pa_ind - out_pu4pa

        # disspu_dom
        s['_disspu_dom'] = disspu_ind - out_disspu
        
        # pu_ind2disspu arrow
        s['pu_ind2disspu'] = s['_disspu_dom']
        
        # pu4pa_trade nodes
        s['_pu4pa_trade'] = data.material.data[g.chempu].tosv(settings, info, g).net_import +\
                                data.material.data[g.semichempu].tosv(settings, info, g).net_import +\
                                data.material.data[g.mechpu].tosv(settings, info, g).net_import
        
        # sawnw_trade nodes
        s['_sawnw_trade'] = data.material.data[g.sawc].tosv(settings, info, g).net_import +\
                                data.material.data[g.sawnc].tosv(settings, info, g).net_import
        
        # plyven_trade nodes
        s['_plyven_trade'] = data.material.data[g.ply].tosv(settings, info, g).net_import +\
                                data.material.data[g.venshe].tosv(settings, info, g).net_import
        
        # fibboa_trade nodes
        s['_fibboa_trade'] = data.material.data[g.fibboa].tosv(settings, info, g).net_import
        
        # partboa_trade nodes
        s['_partboa_trade'] = data.material.data[g.partboa].tosv(settings, info, g).net_import +\
                                data.material.data[g.osb].tosv(settings, info, g).net_import
        
        # disspu_trade nodes
        s['_disspu_trade'] = data.material.data[g.disspu].tosv(settings, info, g).net_import

        # pap_ind node
        s['pap_ind'] = data.material.data[g.cartboa].production +\
                        data.material.data[g.casemat].production +\
                        data.material.data[g.sanipa].production +\
                        data.material.data[g.newsp].production +\
                        data.material.data[g.othpapnes].production +\
                        data.material.data[g.othpack].production +\
                        data.material.data[g.pripamech].production +\
                        data.material.data[g.pripafree].production +\
                        data.material.data[g.pripacoat].production +\
                        data.material.data[g.wrappa].production
        
        # pap domestic
        s['_pap_dom'] = data.material.data[g.cartboa].tosv(settings, info, g).production +\
                        data.material.data[g.casemat].tosv(settings, info, g).production +\
                        data.material.data[g.sanipa].tosv(settings, info, g).production +\
                        data.material.data[g.newsp].tosv(settings, info, g).production +\
                        data.material.data[g.othpapnes].tosv(settings, info, g).production +\
                        data.material.data[g.othpack].tosv(settings, info, g).production +\
                        data.material.data[g.pripamech].tosv(settings, info, g).production +\
                        data.material.data[g.pripafree].tosv(settings, info, g).production +\
                        data.material.data[g.pripacoat].tosv(settings, info, g).production +\
                        data.material.data[g.wrappa].tosv(settings, info, g).production
        
        # pap_trade nodes
        s['_pap_trade'] = data.material.data[g.cartboa].tosv(settings, info, g).net_import +\
                            data.material.data[g.casemat].tosv(settings, info, g).net_import +\
                            data.material.data[g.sanipa].tosv(settings, info, g).net_import +\
                            data.material.data[g.newsp].tosv(settings, info, g).net_import +\
                            data.material.data[g.othpapnes].tosv(settings, info, g).net_import +\
                            data.material.data[g.othpack].tosv(settings, info, g).net_import +\
                            data.material.data[g.pripamech].tosv(settings, info, g).net_import +\
                            data.material.data[g.pripafree].tosv(settings, info, g).net_import +\
                            data.material.data[g.pripacoat].tosv(settings, info, g).net_import +\
                            data.material.data[g.wrappa].tosv(settings, info, g).net_import

        # recpap domestic
        s['_recpap'] = data.material.data[g.recpap].production
        
        # recpap_trade nodes
        s['_recpap_imp'] = data.material.data[g.recpap].import_quantity
        s['_recpap_exp'] = data.material.data[g.recpap].export_quantity
        s['_recpap_trade'] = data.material.data[g.recpap].net_import
        
        # recpap yield considering trade too
        s['_recpap_yield'] = data.material.data[g.recpap].tosv(settings, info, g).production +\
                              data.material.data[g.recpap].tosv(settings, info, g).net_import
        
        # recpu_trade nodes
        s['_recpu_trade'] = data.material.data[g.recpu].net_import
        
        # recpap2pap_ind arrow
        s['recpap2pap_ind'] = data.material.data[g.recpap].tosv(settings, info, g).production +\
                                data.material.data[g.recpap].tosv(settings, info, g).net_import
        
        return s
        


class DetailedSankey(dict):
    """
    Class for the data of a Sankey diagram.
    """
    
    def __init__(self, core):
        zero = FloatUnit(0,core['_energy'].unit)
        null = FloatUnit(None,core['_energy'].unit)
        
        for k in core:
            if k[0]!='_':
                self[k] = core[k]

       # byprod_trade nodes
        byprod_trade = core['_byprod_imp'] - core['_byprod_exp']
        self['byprod_nimp'] = byprod_trade if byprod_trade>zero else null
        self['byprod_nexp'] = -byprod_trade if byprod_trade<=zero else null
        
        # byprod_trade<->byprod arrows
        self['byprod_nimp2byprod'] = self['byprod_nimp']
        self['byprod2byprod_nexp'] = self['byprod_nexp']

        # pel_trade nodes
        pel_trade =  core['_pel_imp'] - core['_pel_exp']
        self['pel_nimp'] = pel_trade if pel_trade>zero else null
        self['pel_nexp'] = -pel_trade if pel_trade<=zero else null
        
        # _pelminexp: amount to add to ene_tot
        # indeed in WRB H&P total doesn't include export of energy biomass
        if self['pel_nexp'].is_zero_or_null():
            _pelminexp = zero
        else:
            _pelminexp = min(self['pel_ind'], self['pel_nexp'])
        _halfpelminexp = Coeff(value=0.5) * _pelminexp
        
        # pel_nimp2energy arrow
        self['pel_nimp2energy'] = self['pel_nimp']
        
        # pel_ind2pel_nexp arrow
        self['pel_ind2pel_nexp'] = self['pel_nexp']

        # pel_ind2energy arrow
        _pel_domproduse = self['pel_ind'] - self['pel_nexp']
        self['pel_ind2energy'] = max(_pel_domproduse, zero)

        _pel_av = self['pel_nimp2energy'] + self['pel_ind2energy']  # available
        if _pel_av > core['_energy']:
            print('Attention: total available pellet is larger than total energy!')
            # raise
            
        # rw_tot2ene_tot arrow & unk_ene node
        # bark2ene_tot arrow and others
        _bark_not4mat = core['_bark_tot'] - core['_bark4pan'] - core['_bark4pu']
        _byp_inp = self['byprod_nimp2byprod'] +\
                       self['pan_ind2byprod'] +\
                       self['saw_ind2byprod'] +\
                       self['pu_ind2byprod']
        _part_byp_out = self['byprod2byprod_nexp'] +\
                           self['byprod2pan_ind'] +\
                           self['byprod2pu_ind']
        _byp_diff = _byp_inp - _part_byp_out # what remains for energy use
        _sec2ene = core['_ind'] -\
                      self['pcw2ene_tot'] -\
                      self['pel_nimp2energy'] # all imported pellet is classified as secondary wood
        if _sec2ene.is_zero_or_null():
             _sec2ene = zero
        _sec_dif = _sec2ene - _bark_not4mat
        _posi_byp_diff = max(_byp_diff,zero)
        # case 100% unknown wood for energy
        if core['_energy'] == core['_unk']:
            # print('a')
            self['unk_ene'] = core['_unk'] + _pelminexp - self['pel_nimp']
            self['rw_tot2ene_tot'] = null
            self['byprod2ene_tot'] = null
            self['bark2ene_tot'] = null
            self['bark2unk_use'] = _bark_not4mat
        else:
            # print('b')
            self['unk_ene'] = core['_unk']
            if core['_ind'].is_zero_or_null():
                # print('c')
                self['rw_tot2ene_tot'] = core['_dir'] + _pelminexp
                # _sec2ene = _sec2ene
            elif core['_dir'].is_zero_or_null():
                # print('d')
                self['rw_tot2ene_tot'] = core['_dir']
                _sec2ene = _sec2ene + _pelminexp
            else:
                # print('e')
                self['rw_tot2ene_tot'] = core['_dir'] + _halfpelminexp
                _sec2ene = _sec2ene + _halfpelminexp
            if _sec2ene <= zero:
                # print('f')
                self['byprod2ene_tot'] = null
                self['bark2ene_tot'] = null
                if self['unk_ene'] > zero:
                    # print('f1')
                    _sec2ene += min(abs(_sec2ene), self['unk_ene'])
                if self['rw_tot2ene_tot'] >= _sec2ene:
                    # print('f2', _sec2ene)
                    self['rw_tot2ene_tot'] += _sec2ene
            else:
                if _sec_dif >= _posi_byp_diff:
                    # print('g')
                    self['byprod2ene_tot'] = _sec2ene - _bark_not4mat
                    self['bark2ene_tot'] = _bark_not4mat
                elif _sec_dif < _posi_byp_diff:
                    # print('h')
                    self['byprod2ene_tot'] = min(_sec2ene,_posi_byp_diff)
                    self['bark2ene_tot'] = _sec2ene - self['byprod2ene_tot']
        
        # uncateg_wood2ene_tot arrow
        self['unk_ene2ene_tot'] = self['unk_ene']

        # ene_tot node
        self['ene_tot'] = self['rw_tot2ene_tot'] +\
                            self['byprod2ene_tot'] +\
                            self['bark2ene_tot'] +\
                            self['pcw2ene_tot'] +\
                            self['unk_ene2ene_tot']
                            
        # ene_tot2energy arrow
        self['ene_tot2energy'] = self['ene_tot'] -\
                                    self['ene_tot2pel_ind']

        # energy node
        self['energy'] = self['ene_tot2energy'] +\
                            self['pel_ind2energy'] +\
                            self['pel_nimp2energy']
        
        if abs(self['energy'] - core['_energy']).value > 0.001:
            print('Check: energy final node differs from WRB energy: ', 
                    self['energy'].value, 
                    core['_energy'].value)

        # bark2unk_use flow
        self['bark2unk_use'] = max(_bark_not4mat - self['bark2ene_tot'], zero)

        _byp_out = self['byprod2byprod_nexp'] +\
                       self['byprod2pan_ind'] +\
                       self['byprod2pu_ind'] +\
                       self['byprod2ene_tot']
        if _byp_inp >= _byp_out:
            self['byprod'] = _byp_inp
            self['unk_byp'] = null
            self['unk_byp_use'] = _byp_inp - _byp_out
        else:
            self['byprod'] = _byp_out
            self['unk_byp'] = _byp_out - _byp_inp
            self['unk_byp_use'] = null
            
        # unk_byp2byprod arrow
        self['unk_byp2byprod'] = self['unk_byp']
        
        # byprod2unk_byp_use arrow
        self['byprod2unk_byp_use'] = self['unk_byp_use']
        
        # rw_rem2rw_tot arrow (overbark)
        self['rw_rem2rw_tot'] = self['rw_rem']

        # rw_trade nodes
        rem_trade = core['_rw_imp'] - core['_rw_exp']
        self['rw_nimp'] = rem_trade if rem_trade>=zero else null
        self['rw_nexp'] = -rem_trade if rem_trade<=zero else null
        
        # rw_trade<->rw_tot arrows (overbark)
        self['rw_nimp2rw_tot'] = self['rw_nimp']
        self['rw_tot2rw_nexp'] = self['rw_nexp']

        # rw_tot, unk_pri, unk_pri_use nodes
        rw_inp = self['rw_nimp2rw_tot'] +\
                  self['rw_rem2rw_tot']
        rw_out = self['bark2ene_tot'] +\
                  self['bark2unk_use'] +\
                  self['rw_tot2ene_tot'] +\
                  self['rw_tot2rw_nexp'] +\
                  core['rw_tot2rw4mat']
        if rw_inp > rw_out:
            self['rw_tot'] = rw_inp
            self['unk_pri'] = null
            self['unk_pri_use'] = rw_inp - rw_out
        elif rw_inp < rw_out:
            self['rw_tot'] = rw_out
            self['unk_pri'] = rw_out - rw_inp
            self['unk_pri_use'] = null
        else:
            self['rw_tot'] = rw_inp
            self['unk_pri'] = null
            self['unk_pri_use'] = null
            
        # unk_pri2rw_tot arrow
        self['unk_pri2rw_tot'] = self['unk_pri']
        
        # rw_tot2unk_pri_use arrow
        self['rw_tot2unk_pri_use'] = self['unk_pri_use']
        
        # sawnw_trade nodes
        self['sawnw_nimp'] = core['_sawnw_trade'] if core['_sawnw_trade'] > zero else null 
        self['sawnw_nexp'] = -core['_sawnw_trade'] if core['_sawnw_trade'] <= zero else null 
        
        # saw_ind2sawnw arrows
        self['sawnw_nimp2sawnw'] = self['sawnw_nimp']
        self['sawnw2sawnw_nexp'] = self['sawnw_nexp']
      
        # plyven_trade nodes
        self['plyven_nimp'] = core['_plyven_trade'] if core['_plyven_trade'] > zero else null 
        self['plyven_nexp'] = -core['_plyven_trade'] if core['_plyven_trade'] <= zero else null 
        
        # plyven_ind2plyven arrows
        self['plyven_nimp2plyven'] = self['plyven_nimp']
        self['plyven2plyven_nexp'] = self['plyven_nexp']

       # partboa_trade nodes
        self['partboa_nimp'] = core['_partboa_trade'] if core['_partboa_trade'] > zero else null 
        self['partboa_nexp'] = -core['_partboa_trade'] if core['_partboa_trade'] <= zero else null 
        
        # partboa_ind2partboa arrows
        self['partboa_nimp2partboa'] = self['partboa_nimp']
        self['partboa2partboa_nexp'] = self['partboa_nexp']

        # fibboa_trade nodes
        self['fibboa_nimp'] = core['_fibboa_trade'] if core['_fibboa_trade'] > zero else null 
        self['fibboa_nexp'] = -core['_fibboa_trade'] if core['_fibboa_trade'] <= zero else null 
        
        # fibboa_ind2fibboa arrows
        self['fibboa_nimp2fibboa'] = self['fibboa_nimp']
        self['fibboa2fibboa_nexp'] = self['fibboa_nexp']

         # disspu_trade nodes
        self['disspu_nimp'] = core['_disspu_trade'] if core['_disspu_trade'] > zero else null 
        self['disspu_nexp'] = -core['_disspu_trade'] if core['_disspu_trade'] <= zero else null 
        
        # disspu_ind2disspu arrows
        self['disspu_nimp2disspu'] = self['disspu_nimp']
        self['disspu2disspu_nexp'] = self['disspu_nexp']
        
        # pu4pa_trade nodes
        self['pu4pa_nimp'] = core['_pu4pa_trade'] if core['_pu4pa_trade'] > zero else null
        self['pu4pa_nexp'] = -core['_pu4pa_trade'] if core['_pu4pa_trade'] <= zero else null
                
        # pap_trade nodes
        self['pap_nimp'] = core['_pap_trade'] if core['_pap_trade'] > zero else null
        self['pap_nexp'] = -core['_pap_trade'] if core['_pap_trade'] <= zero else null

        # pap_trade<->pap
        self['pap_nimp2pap'] = self['pap_nimp']
        self['pap2pap_nexp'] = self['pap_nexp']
        
        # pap node
        self['pap'] = core['_pap_dom'] + self['pap_nimp2pap']

        # recpap_trade nodes
        self['recpap_nimp'] = core['_recpap_trade'] if core['_recpap_trade'] > zero else null
        self['recpap_nexp'] = -core['_recpap_trade'] if core['_recpap_trade'] <= zero else null

        # pap_ind2pap arrow
        self['pap_ind2pap'] = core['_pap_dom']

        # papbyp node
        self['pap_byp'] = core['pap_ind'] - core['_pap_dom']
        
        # pap_ind2pap_byp arrow
        self['pap_ind2pap_byp'] = self['pap_byp']

        # recpap_trade<->pap
        self['recpap_nimp2recpap'] = self['recpap_nimp']
        self['recpap2recpap_nexp'] = self['recpap_nexp']

        # pap_byp2recpap arrow
        self['pap_byp2recpap'] = min(self['pap_byp'], core['_recpap'])

        # pap2recpap arrow
        self['pap2recpap'] =  core['_recpap'] - self['pap_byp2recpap']

        # and recpu_nimp2pu4pa arrow
        if core['_recpu_trade']>zero:
            self['recpu_nimp2pu4pa'] = core['_recpu_trade']
            self['recpap2recpu_nexp'] = null
        else:
            self['recpu_nimp2pu4pa'] = null
            self['recpap2recpu_nexp'] = -core['_recpu_trade']
        
        # pu4pa_trade<->pu4pa arrow
        self['pu4pa_nimp2pu4pa'] = self['pu4pa_nimp']
        self['pu4pa2pu4pa_nexp'] = self['pu4pa_nexp']

        # pu4pa node
        self['pu4pa'] = self['pu_ind2pu4pa'] + self['pu4pa_nimp2pu4pa'] + self['recpu_nimp2pu4pa']

        # pu4pa2pap_ind arrow
        self['pu4pa2pap_ind'] = max(self['pu4pa'] - self['pu4pa2pu4pa_nexp'], zero)

        # recpap2pap_ind arrow
        self['recpap2pap_ind'] = max(core['_recpap_yield'] -\
                                     self['recpap2recpu_nexp'],
                                     zero)
        
        # final check for negative values
        for k in self:
            if self[k]<zero:
                print('----------- Error: {0} is negative: {1}'.format(k,self[k].value))
                # alt
        
    def toxls(self, settings, info, g):
        """
        write a whole Sankey to an xls file

        Parameters
        ----------
        info : Info
            see std.
        g : container globset.py
            global variables.

        Returns
        -------
        None.

        """

        filename = os.path.join(settings.outfile_path, 'Forestry_Sankey_' + str(info.year) + '.xlsx')
        try:
            wb = xl.load_workbook(filename)
        except:
            wb = xl.Workbook()
            
        try:
            ws = wb[info.country]
        except:
            ws = wb.create_sheet(title=info.country)
        
        unit = 'unit: thousands cubic meters SWE'
            
        ws.cell(row=1, column=1, value='{0}  ({1})'.format(info.country,str(info.year)))
        ws.cell(row=2, column=1, value=unit)
        
        r = 3
        for k in sorted(self.keys()):
            if '2' in k:
                r+=1
                ws.cell(row=r, column=1, value=k)
                ws.cell(row=r, column=2, value=round(self[k],1).value)

        wb.save(filename)
        return
